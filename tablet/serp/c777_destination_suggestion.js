
import { Selector } from 'testcafe'

const frontendUrl = process.env.FRONTEND_URL || 'http://127.0.0.1:3000'
fixture`Getting Started`
    .page`${frontendUrl}/ru/search?priceMin=5000`;

test('C777 Подсказки направлений после ввода направления', async t => {

    await t  //вводим часть названия страны
        .typeText(Selector('#pre-filters-destination input[type="text"]'), 'Gr')
        .wait(1000)
        .expect(Selector('#pre-filters-destination').innerText).contains('Greece')
        .expect(Selector('#pre-filters-destination').innerText).notContains('Mediterranean')
        //вводим название целиком
        .typeText(Selector('#pre-filters-destination input[type="text"]'), 'Greece', { replace: true })
        .wait(1000)
        .expect(Selector('#pre-filters-destination').innerText).contains('Greece')
        .expect(Selector('#pre-filters-destination').innerText).notContains('Mediterranean')
        //вводим название капсом
        .typeText(Selector('#pre-filters-destination input[type="text"]'), 'GREECE', { replace: true })
        .wait(1000)
        .expect(Selector('#pre-filters-destination').innerText).contains('Greece')
        .expect(Selector('#pre-filters-destination').innerText).notContains('Mediterranean')
        //вводим название маленькими
        .typeText(Selector('#pre-filters-destination input[type="text"]'), 'greece', { replace: true })
        .wait(1000)
        .expect(Selector('#pre-filters-destination').innerText).contains('Greece')
        .expect(Selector('#pre-filters-destination').innerText).notContains('Mediterranean')
        //очищаем строку поиска
        .pressKey('ctrl+a delete')
        .wait(1000)
        .expect(Selector('#pre-filters-destination').innerText).contains('Mediterranean')
        .expect(Selector('#pre-filters-destination').innerText).contains('Greece')
}); 